from django.shortcuts import render
from django.http import HttpResponseNotFound

from django.views.generic.dates import ArchiveIndexView, TodayArchiveView

from .models import Session

import datetime

class SessionsToday(TodayArchiveView):
    template_name='clock/today.html'
    queryset=Session.objects.all()
    date_field='date'
    allow_future=True
    allow_empty = True

class Archive(ArchiveIndexView):
    model=Session
    template_name='clock/session_archive.html'
    date_field='date'
    allow_future=True

    def get_queryset(self):
        return Session.objects.order_by('-date')

def sessions_on_date(request,d,m,y):
    date_=datetime.date(day=d,month=m,year=y)
    sessions=Session.objects.filter(date=date_)
    if sessions.count() > 0:
        next_session=sessions.order_by('date')[0]
        venue=next_session.venue
        country=venue.country
        title="index"
        context={
            'sessions_list':sessions,
            'next_session':next_session,
            'venue':venue,
            'country':country,
            'title':title
        }
        return render(request, 'clock/index.html', context)
    else:
        return HttpResponseNotFound('Not race week yet boi')

def today(request):
    today=datetime.date.today()
    return sessions_on_date(day=today.day,month=today.month,year=today.year)

def index(request):
    return sessions_on_date(request,30,8,2019)
