from django.urls import path

from .models import Session

from . import views

urlpatterns = [
        path('',views.index,name='index'),
        path('archive/', views.Archive.as_view(), name="session_archive"),
        path('today/', views.SessionsToday.as_view(), name="today"),
]
