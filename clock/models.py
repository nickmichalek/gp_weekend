from django.db import models
from django_countries.fields import CountryField
from .utils import min_value_validator, max_value_this_year
from django.urls import reverse

from django.utils.translation import ugettext_lazy as _

class Venue(models.Model):
    name = models.CharField(blank=False,max_length=255)
    country = CountryField()
    year = models.PositiveIntegerField(_('year'),validators=[min_value_validator, max_value_this_year])
    track = models.CharField(blank=False,max_length=255)

    class Meta:
        #indexes = [models.Index(fields=['full_name'])]
        #ordering = ['-full_name']
        verbose_name='Venue'
        verbose_name_plural='Venues'

    def __str__(self):
        return str(self.year)+' '+self.name
    
    #def clean(self):

    #def save(safe):

    def get_absolute_url(self):
        return reverse('venue_detail', args=[str(self.id)])

class Session(models.Model):
    DURATIONS={
        'FP1':90,
        'FP2':90,
        'FP3':60,
        'Q1':18,
        'Q2':15,
        'Q3':12,
        'Race':120
    }
    SESSIONS=(
            ('FP1','FP1'),
            ('FP2','FP2'),
            ('FP3','FP3'),
            ('Q1','Q1'),
            ('Q2','Q2'),
            ('Q3','Q3'),
            ('Race','Race')
    )
    venue=models.ForeignKey(Venue, on_delete=models.CASCADE)
    session=models.CharField(blank=False,max_length=4,choices=SESSIONS)
    date=models.DateField(blank=False)
    start=models.TimeField(blank=False)
    #start=models.DateTimeField(blank=False)
    duration=models.DurationField(blank=False)

    def __str__(self):
        return str(self.venue)+" "+self.session

    def session_in_future(self):
        today=datetime.date.today()
        now=datetime.datetime.now()
        return today<=self.date and now<self.start
        #return now < self.start

class SessionInterrupt(models.Model):
    session=models.ForeignKey(Session, on_delete=models.CASCADE)
    will_resume=models.BooleanField(blank=False,default=True)
    resume_at=models.TimeField()

    def __str__(self):
        return "Session will resume at "+self.resume_at.strftime("%H:%M") if self.will_resume else "Session will not resume"

class SessionDelay(SessionInterrupt):
   pass 
