import datetime
from django.core.validators import MaxValueValidator,MinValueValidator

def this_year():
    return datetime.date.today().year

def min_value_validator(self):
    return MinValueValidator(1950)

def max_value_this_year(value):
    return MaxValueValidator(this_year())(value)

def years_range():
    return [(r,r) for r in range(1950,this_year+1)]
