from django.contrib import admin

from .models import Venue,Session,SessionInterrupt

admin.site.register(Venue)
admin.site.register(Session)
admin.site.register(SessionInterrupt)
